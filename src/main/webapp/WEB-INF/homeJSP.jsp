<%-- 
    Document   : homeJSP
    Created on : 11 févr. 2020, 23:36:07
    Author     : abdis
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <title>MQTTServerNode</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div>
            <h1>Welcome to the MQTT Server node</h1>
            <p>From here you can search messages from a certain topic</p>
            <form action="/MQTTWeb/home" method="post">
                <label>Enter a topic name: </label>
                <input type="text" name="topic_name" <% if(request.getParameter("topic_name")!=null) out.print("value=\"" + request.getParameter("topic_name") + "\""); %>/>
                <input type="submit" value="Submit"/>
            </form>
        </div>
        <div id="messageDisplayBox">
            <%
                out.println(request.getAttribute("content"));
            %>
        </div>
    </body>
</html>