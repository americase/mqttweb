package com.home.mqtt.MQTTWeb;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author abdis
 */
public class Main {
    
    public static String BROKER_ADDRESS = "tcp://192.168.1.55:1883";
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            db.DBManager.getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
