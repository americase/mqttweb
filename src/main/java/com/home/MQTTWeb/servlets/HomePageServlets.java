/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.home.MQTTWeb.servlets;

import com.home.MQTTWeb.ErrorMessage;
import com.home.MQTTWeb.Message;
import db.MessageTable;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author abdis
 */
public class HomePageServlets extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String nodeId = request.getParameter("node_id");
        if (nodeId != null) {
            request.setAttribute("content", createNodeSearchContent(nodeId));
        } else {
            request.setAttribute("content", "");
        }
        this.getServletContext().getRequestDispatcher("/WEB-INF/homeJSP.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String topicName = (String) request.getParameter("topic_name");
        String content = createTopicSearchContent(topicName);
        request.setAttribute("content", content);
        this.getServletContext().getRequestDispatcher("/WEB-INF/homeJSP.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

    private String createNodeSearchContent(String nodeId) {
        if (nodeId == null) {
            return new ErrorMessage("Null pointer!").toString();
        }
        if(nodeId.isEmpty()){
            return new ErrorMessage("Please enter a Sensor ID").toString();
        }
        List<Message> messages = new ArrayList<>();
        try {
            List<Message >messages2 = MessageTable.getAllMessage();
            for (Message message : messages2) {
                if(message.getSensorID().equals(nodeId)){
                    messages.add(message);
                }
            }
        } catch (SQLException ex) {
            return new ErrorMessage("Database error\n" + ex.getMessage()).toString();
        }
        String content = "<div><p>There is " + messages.size() + " messages sent by node: " + nodeId + "</p></div>";
        for (int i = 0; i < messages.size(); i++) {
            content += createContentLine(messages.get(i));
        }
        content += "</p>";
        return content;
    }

    private String createTopicSearchContent(String topic) {
        if (topic == null) {
            return new ErrorMessage("Null pointer!").toString();
        }
        if (topic.isEmpty()) {
            return new ErrorMessage("Please enter a message topic").toString();
        }
        List<Message> messages;
        try {
            messages = MessageTable.getMessage(topic);
        } catch (SQLException ex) {
            return new ErrorMessage("Database error\n" + ex.getMessage()).toString();
        }
        String content = "<div><p>There is " + messages.size() + " messages with topic name: " + topic + "</p></div>";
        for (int i = 0; i < messages.size(); i++) {
            content += createContentLine(messages.get(i));
        }
        content += "</p>";
        return content;
    }

    private String createContentLine(Message message) {
        String sensorId = message.getSensorID();
        String topicName = message.getTopic();
        String temp = message.getContent();
        return "<div>Message sent by node: <a href=\"/MQTTWeb/home?node_id=" + sensorId + "\">" + sensorId + "</a>with the message " + temp + " on topic:"+topicName+"</div>";
    }

}
