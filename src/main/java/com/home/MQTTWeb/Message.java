/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.home.MQTTWeb;

/**
 *
 * @author abdis
 */
public class Message {

    private final String sensorID;
    private final String content;
    private final String topic;

    public Message(String sensorID, String topic, String content) {
        this.sensorID = sensorID;
        this.content = content;
        this.topic = topic;
    }

    public String getSensorID() {
        return sensorID;
    }

    public String getContent() {
        return content;
    }

    public String getTopic() {
        return topic;
    }

    @Override
    public String toString() {
        return "Message{" + "sensorID=" + sensorID + ", content=" + content + ", topic=" + topic + '}';
    }
}
