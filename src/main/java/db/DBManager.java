/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author abdis
 */
public class DBManager {
    
    private static Connection connection;

    /**
     * Return an established connection to the DB.
     *
     * @return the connection.
     * @throws SQLException when connection error.
     */
    public static Connection getConnection() throws SQLException {
        if (connection == null) {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/nodecom?useUnicode=true &useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false& serverTimezone=UTC", "root", "");
            return connection;
        } else {
            return connection;
        }
    }
    
    public static void startTransaction(Integer isolationLevel) throws SQLException {
        if (isolationLevel == null) {
            getConnection().setAutoCommit(false);
            return;
        }
        
        int isol = 0;
        switch (isolationLevel) {
            case 0:
                isol = java.sql.Connection.TRANSACTION_READ_UNCOMMITTED;
                break;
            case 1:
                isol = java.sql.Connection.TRANSACTION_READ_COMMITTED;
                break;
            case 2:
                isol = java.sql.Connection.TRANSACTION_REPEATABLE_READ;
                break;
            case 3:
                isol = java.sql.Connection.TRANSACTION_SERIALIZABLE;
                break;
            default:
                throw new SQLException("Degré d'isolation inexistant!");
        }
        
        getConnection().setTransactionIsolation(isol);
        
    }
    
    public static void validateTransaction() throws SQLException{
        getConnection().commit();
        getConnection().setAutoCommit(true);
    }
    
    public static void rollBackTransaction() throws SQLException{
        getConnection().rollback();
        getConnection().setAutoCommit(true);
    }
    
}
