package db;

import com.home.MQTTWeb.Message;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author abdis
 */
public class MessageTable {
    
    public static List<Message> getMessage(String topic) throws SQLException {
        List<Message> messages = new ArrayList<>();
        String prepapredQuery = prepareSQLQuery(topic);
        PreparedStatement statement = DBManager.getConnection().prepareStatement(prepapredQuery);
        prepareStatement(topic, statement);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            String sensorID = resultSet.getString("NODE_ID");
            String messageContent = resultSet.getString("Message");
            Message msg = new Message(sensorID, topic, messageContent);
            messages.add(msg);
        }
        return messages;
    }

    public static List<Message> getAllMessage() throws SQLException {
        return getMessage(null);
    }

    private static String prepareSQLQuery(String topic) {
        String prep = "SELECT * from mqtt_messages ";
        String where = "";
        if (topic != null) {
            if (!topic.isEmpty()) {
                where += "WHERE TOPIC_NAME= ? ";
            }
        }
        prep += where;
        return prep;
    }

    private static void prepareStatement(String topic, PreparedStatement statement) throws SQLException {
        if (topic != null) {
            if (!topic.isEmpty()) {
                statement.setString(1, topic);
            }
        }
    }

    public static void insertMessage(Message message) throws SQLException {
        PreparedStatement statement = DBManager.getConnection().prepareStatement("INSERT into mqtt_messages(NODE_ID, TOPIC_NAME, MESSAGE) values(?,?,?)");
        statement.setString(1, message.getSensorID());
        statement.setString(2, message.getTopic());
        statement.setString(3, message.getContent());
        statement.execute();
    }

}
